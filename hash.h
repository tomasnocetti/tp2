#ifndef HASH_H
#define HASH_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


/* *****************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

/* Se trata de una lista que contiene datos de tipo void*
 * (punteros genéricos).  La lista en sí está definida en el .c.  */

typedef struct hash hash_t;
typedef struct hash_iter hash_iter_t;
typedef void (*hash_destruir_dato_t)(void *);


/* *****************************************************************
 *                    PRIMITIVAS DE LA LISTA
 * *****************************************************************/


// Crea un hash.
// Pre: Destruir_dato es una función capaz de destruir
// los datos del hash, o NULL en caso de que no se la utilice.
// Post: devuelve un nuevo hash vacío.
hash_t *hash_crear(hash_destruir_dato_t destruir_dato);

// Destruye el hash.
// PRE: el hash fue creado.
void hash_destruir(hash_t *hash);

// Devuelve verdadero o falso, según si el hash tiene o no el elemento guardado con esa clave.
// Pre: el hash fue creada.
bool hash_pertenece(const hash_t *hash, const char *clave);

// Agrega un nuevo elemento en la primera posicion de la lista.
// Pre: la lista fue creada.
// Post: se agregó un nuevo elemento al principio de la lista.
// Devuelve falso en caso de error y verdadero si todo sale correctamente.
bool hash_guardar(hash_t *hash, const char *clave, void *dato);

// Saca el elemento que posea la clave. Si el hash no posee el elemento, devuelve
// NULL.
// Pre: el hash fue creado.
// Post: si la clave esta en el hash se devuelve el valor y se elimina del hash, de lo contrario NULL.
void *hash_borrar(hash_t *hash, const char *clave);

// Devuelve la cantidad de elementos del hash.
// Pre: el hash fue creado.
size_t hash_cantidad(const hash_t *hash);

// Devuelve el valor del hash con la clave pasada por parametro.
// Pre: el hash fue creado.
// Post: si la clave se encuentra en el hash devuelve su valor, de lo contrario NULL
void *hash_obtener(const hash_t *hash, const char *clave);

/* *****************************************************************
 *                    PRIMITIVAS DEL ITERADOR EXTERNO
 * *****************************************************************/

// Crea un iterador del hash.
// Pre: el hash esta creado.
// Post: devuelve un nuevo iterador.
hash_iter_t *hash_iter_crear(const hash_t *hash);

// Avanza a la siguiente posicion del iterador.
// Pre: el iterador esta creado.
// Post: mueve la referencia actual del iterador al siguiente elemento. Si
// el iterador se encuentra en el final entonces devuelve false, de lo contrario
// devuelve true.
bool hash_iter_avanzar(hash_iter_t *iter);

// Obtiene el valor de la clave en la posicion actual del iterador. Si el iterador no esta al final,
// se devuelve la clave. Si el iterador esta al final devuelve NULL.
// Pre: el iterador esta creado.
// Post: se devolvió la clave de la posicion actual del iterador.
const char *hash_iter_ver_actual(const hash_iter_t *iter);

// Devuelve verdadero o falso, según si el iterador se encuentra al final.
// Pre: el iterador fue creado.
// Post: si el iterador esta al final devuelve verdadero, en el caso contrario 
// devuelve falso.
bool hash_iter_al_final(const hash_iter_t *iter);

// Destruye el iterador.
// Pre: el iterador fue creado.
// Post: el iterador fue destruido.
void hash_iter_destruir(hash_iter_t* iter);


/* *****************************************************************
 *                    PRUEBAS PARA EL ITERADOR
 * *****************************************************************/

// Realiza pruebas sobre la implementación del iterador del alumno.
//
// Las pruebas deben emplazarse en el archivo ‘pruebas_alumno.c’, y
// solamente pueden emplear la interfaz pública tal y como aparece en lista.h
// (esto es, las pruebas no pueden acceder a los miembros del struct lista).
//
// Para la implementación de las pruebas se debe emplear la función
// print_test(), como se ha visto en TPs anteriores.
void pruebas_hash_alumno(void);

#endif // HASH_H
