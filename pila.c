#include "pila.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define MIN 10
#define RED 2


/* Definición del struct pila proporcionado por la cátedra.
 */
struct pila {
    void** datos;
    size_t cantidad;  // Cantidad de elementos almacenados.
    size_t capacidad;  // Capacidad del arreglo 'datos'.
};

/* Funcion que crea la pila vacia.
	Post: Pila creada.*/
pila_t* pila_crear(void) {
	pila_t* pila = malloc(sizeof(pila_t));
	if (pila == NULL) {
		return NULL;
	}

	pila->datos=malloc(sizeof(void*) * MIN);

	if (pila->datos == NULL) {
		free(pila);
		return NULL;
	}
	pila->capacidad = MIN;
	pila->cantidad = 0;
	return pila;
}

/* Funcion que destruye la pila.
 	Pre: la pila fue creada.
 	Post: la pila fue destruida. */
void pila_destruir(pila_t* pila) {
	free(pila->datos);
	free(pila);
}

/* Funcion que cambia la dimension de pila. Devuelve true si se agrando
	y false en caso de error.
	Pre: pila fue creada.
	Post: La pila fu redimensionada.*/
bool _pila_redimensionar(pila_t* pila, size_t tamanio_nuevo) {
	void* datos_nuevo = realloc(pila->datos, sizeof(void*) * (tamanio_nuevo));
	if (tamanio_nuevo > 0 && datos_nuevo == NULL) {
		return false;
	}
	pila->datos = datos_nuevo;
	pila->capacidad = tamanio_nuevo;
	return true;
}

/* Funcion que evalua si la pila esta vacia. Devuelve true si lo esta y false si no lo esta.
	Pre: La pila fue creada.*/
bool pila_esta_vacia(const pila_t *pila) {
	return pila->cantidad == 0;
}

/* Funcion que apila un elemento en la pila. Devuelve True si se pudo apilar
	y false en caso de error.
	Pre: La pila fue creada.
	Post: el elemento fue apilado.*/
bool pila_apilar(pila_t* pila, void* valor) {
	if (pila->cantidad == pila->capacidad) {
		if (!_pila_redimensionar(pila,pila->capacidad * RED)) { 
			return false;
		}
	}
	pila->datos[pila->cantidad] = valor;
	pila->cantidad ++;
	return true;
}

/* Funcion que devuelve el elemento de arriba de toda la pila. Si la pila esta vacia devuelve NULL
	Pre: la pila fue creada.
	Post: Se devolvio el elemento sin desapilaro o NULL en caso de estar vacia.*/
void* pila_ver_tope(const pila_t* pila) {
	if (pila_esta_vacia(pila)) {
		return NULL;
	}
	return pila->datos[pila->cantidad - 1];
}

/* Funcion que desapila elemento de la pila. Desapila y devuelve el element, si esta vacia
	Devuelve NULL.
	Pre: La pila fue creada.
	Post: Se desapilo el elemento y se devolvio o si esta vacia devuelve NULL.*/
void* pila_desapilar(pila_t *pila) {
	if (pila_esta_vacia(pila)) {
		return NULL;
	}
	pila->cantidad --;
	if (pila->cantidad == pila->capacidad / 4) {
		if (pila->capacidad / RED > MIN) {
			_pila_redimensionar(pila,pila->capacidad / RED);
		}
		else {
			_pila_redimensionar(pila,MIN);	
		}
	}
	return pila->datos[pila->cantidad];
}
