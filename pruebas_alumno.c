#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "dos.h"
#include "testing.h"

void imprimir_prueba(const char* ip) {
  printf("\t%s\n", ip);
}

void visitar_prueba(const char* url, size_t cantidad) {
  printf("\t%s - %lu\n", url, cantidad);
}

static void pruebas_iniciar_sistema() { 
	sistema_t* sistema = iniciar_sistema();

	print_test("Ver los sitios mas visitados, no imprime nada", true );
	ver_mas_visitados(sistema, 2, visitar_prueba);
	printf("\n");

	print_test("Ver visitantes de 0.0.0.0 a 255.255.255.255, no imprime nada", true );
	ver_visitantes(sistema,imprimir_prueba,"0.0.0.0","255.255.255.255");
  printf("\n");
  terminar_sistema(sistema);
}

static void pruebas_cargando_archivo_1() {
	sistema_t* sistema = iniciar_sistema();

	print_test("No imprime ningun DoS, ya que hay una unica diferencia de tiempos de 2 segundos", procesar_archivo(sistema, "archivo1.log"));
	printf("\n");

	print_test("Ver los 10 sitios mas visitados, imprime los unicos 2 del archivo", true );
	ver_mas_visitados(sistema, 10, visitar_prueba);
	printf("\n");	

	print_test("Ver visitantes de 0.0.0.0 a 255.255.255.255, imprime la unica ip, 219.64.34.68", true );
	ver_visitantes(sistema,imprimir_prueba,"0.0.0.0","255.255.255.255");
	printf("\n");


  terminar_sistema(sistema);
}

static void pruebas_cargando_archivo_2() {
	sistema_t* sistema = iniciar_sistema();

	print_test("Imprime los DoS pertinentes en orden de menor a mayor, pero no imprime dos veces el repetido", procesar_archivo(sistema, "archivo2.log"));
	printf("\n");

	print_test("Ver los 10 sitios mas visitados, imprime los 4 visitados del archivo, del mas visitado al menos", true );
	ver_mas_visitados(sistema, 10, visitar_prueba);
	printf("\n");	

	print_test("Ver visitantes de 60.0.0.0 a 69.255.255.255, imprime solo dos ips que se encuentran en ese intervalo, tambien de menor a mayor", true );
	ver_visitantes(sistema,imprimir_prueba,"60.0.0.0","69.255.255.255");
	printf("\n");

	print_test("Ver visitantes de 60.0.0.0 a 60.0.0.0, no imprime nada, ya que lo unico que podria imprimir seria esa ip, que no aparece", true );
	ver_visitantes(sistema,imprimir_prueba,"60.0.0.0","60.0.0.0");
	printf("\n");

	print_test("Ver visitantes de 68.180.224.225 a 68.180.224.225, imprime solo esa ip, que aparece en el archivo", true );
	ver_visitantes(sistema,imprimir_prueba,"68.180.224.225","68.180.224.225");
	printf("\n");

  terminar_sistema(sistema);
}	

static void pruebas_cargando_archivo_3() {
	sistema_t* sistema = iniciar_sistema();

	print_test("No imprime nada, ya que no hay ningun DoS para reportar", procesar_archivo(sistema, "archivo3.log"));
	printf("\n");

	print_test("Ver los 10 sitios mas visitados, imprime los 10 mas visitados del archivo, del mas visitado al menos", true );
	ver_mas_visitados(sistema, 10, visitar_prueba);
	printf("\n");	

	print_test("Ver visitantes de 00.0.0.0 a 255.255.255.255, imprime todas las ips del archivo, tambien de menor a mayor", true );
	ver_visitantes(sistema,imprimir_prueba,"60.0.0.0","69.255.255.255");
	printf("\n");

  terminar_sistema(sistema);
}	

static void pruebas_cargando_archivo_inc() {
	sistema_t* sistema = iniciar_sistema();

	print_test("Procesar devuelve falso ya que no exite el archivo", !procesar_archivo(sistema, "noexiste.log"));
	printf("\n");

	print_test("Ver los sitios mas visitados, no imprime nada", true );
	ver_mas_visitados(sistema, 2, visitar_prueba);
	printf("\n");

	print_test("Ver visitantes de 0.0.0.0 a 255.255.255.255, no imprime nada", true );
	ver_visitantes(sistema,imprimir_prueba,"0.0.0.0","255.255.255.255");
	printf("\n");

  terminar_sistema(sistema);

}


void pruebas_sistema(){
	printf("-------Pruebas inicio del sistema-------\n\n");
  pruebas_iniciar_sistema();
	printf("\n\n-------Pruebas archivo con una sola ip------- \n\n");
  pruebas_cargando_archivo_1();
	printf("\n\n-------Pruebas archivo varias ips y DoS repetido y en desorden------- \n\n");
  pruebas_cargando_archivo_2();
	printf("\n\n-------Pruebas archivo muchas ips distintas y sin DoS para reportar------- \n\n");
	pruebas_cargando_archivo_3();
	printf("\n\n-------Pruebas archivo incorrecto------- \n\n");
	pruebas_cargando_archivo_inc();
}