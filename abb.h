#ifndef ABB_H
#define ABB_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


/*************************************************************
*                 Declaracion de structs                     *
*************************************************************/
typedef struct abb abb_t;

typedef int (*abb_comparar_clave_t) (const char *, const char *);
typedef void (*abb_destruir_dato_t) (void *);

typedef struct abb_iter abb_iter_t;

/*************************************************************
*                 Declaracion funciones abb                  *
*************************************************************/

// Crea el abb, recibiendo como parametro una funcion de comparacion
// y una funcion de destruccion de dato.
// Post: el abb fue creado.
abb_t* abb_crear(abb_comparar_clave_t cmp, abb_destruir_dato_t destruir_dato);

// Guarda un elemento en el arbol con su respectiva clave. Si se guardo el elemento,
// se devuelve true, de caso contrario devuelve false
// Pre: el arbol fue creado.
// Post: El elemento fue guardado y se devuelve true, de caso contrario
// devuelve false
bool abb_guardar(abb_t *arbol, const char *clave, void *dato);

// Borra un elemento del arbol recibiendo como parametro la clave. Se devuelve el dato,
// si se borra, en caso contrario devuelve NULL.
// Pre: El arbol fue creado.
// Post: Se devolvio el dato, y se borro el elemento, en caso contrario se devolvio NULL.
void *abb_borrar(abb_t *arbol, const char *clave);

// Se obtiene el dato asociado a una clave en el arbol. Se devuelve el dato, si se encuentra, 
// en caso contrario devuelve NULL.
// Pre: El arbol fue creado.
// Post: se devolvio el dato si se encontro, sino devuelve NULL
void *abb_obtener(const abb_t *arbol, const char *clave);

// Busca si pertence el elemento en el arbol. Se devuelve el dato, si se encuentra, 
// en caso contrario devuelve false.
// Pre: El arbol fue creado.
// Post: se devolvio true si se encontro, sino devuelve false.
bool abb_pertenece(const abb_t *arbol, const char *clave);

// Devuelve la cantidad de elemento del arbol.
// Pre: el arbol fue creado.
// Post: se devolvio la cantidad de elementos que hay en el arbol
size_t abb_cantidad(abb_t *arbol);

// Destruye ela arbol y si quedaron elementos adentro tambien.
// Pre:  El arbol fue creado.
// Post: El arbol fue destruido.
void abb_destruir(abb_t *arbol);



/*************************************************************
*                 Declaracion funciones iterador             *
*************************************************************/
/*ITERADOR INTERNO*/

// Funcion que recorre el arbol de manera inorder, y aplica una funcion a cada elmento.
// Post: Los elementos sufrieron cambio si es que visitar pudo relizarlo
void abb_in_order(abb_t *arbol, bool visitar(const char *, void *, void *), void *extra);

void abb_claves_in_order(abb_t *arbol, void visitar(const char*), char* desde, char* hasta);

/*ITERADOR EXTERNO, RECORRE DE MANERA INORDER*/

// Crea el iterador
// Post: El iterador fue creado.
abb_iter_t *abb_iter_in_crear(const abb_t *arbol);

// Avanza una posicion en el arbol. Devuelve true si lo pudo hacer, de otra manera, devuleve false.
// Pre: El iterador fue creado
// Post: Se devolvio true si se pudo avanzar, de caso contrario devolvio false.
bool abb_iter_in_avanzar(abb_iter_t *iter);

// Devuelve la clave del elemento en el cual esta parado el iterador
// Pre: El iterador fue creado
// Post: Se devolvio la clave o se devolvio NULL, en caso de no poder leer la clave
const char *abb_iter_in_ver_actual(const abb_iter_t *iter);

// Devuelve true si el iterador esta al final del arbol, de caso contrario devuelve false.
// Pre: El iterador fue creado
// Post: Se devolvio true en caso afirmativo, de otra forma devolvio false
bool abb_iter_in_al_final(const abb_iter_t *iter);

// Destruye el iterador
// Pre: El iterador fue creado.
// Post: el iterador fue borrado.
void abb_iter_in_destruir(abb_iter_t* iter);


/* *****************************************************************
 *                    PRUEBAS PARA EL ABB
 * *****************************************************************/

// Realiza pruebas sobre la implementación de la lista del alumno.
//
// Las pruebas deben emplazarse en el archivo ‘pruebas_alumno.c’, y
// solamente pueden emplear la interfaz pública tal y como aparece en lista.h
// (esto es, las pruebas no pueden acceder a los miembros del struct lista).
//
// Para la implementación de las pruebas se debe emplear la función
// print_test(), como se ha visto en TPs anteriores.
void pruebas_abb_alumno(void);
#endif // ABB_H