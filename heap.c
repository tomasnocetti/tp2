#include "heap.h"
#include <stdbool.h>  /* bool */
#include <stddef.h>	  /* size_t */
#include <stdlib.h>
#include <stdio.h>
#define TAM_INICIAL 50
#define CONST_REDIMENSION 2


struct heap {
	void **arreglo;
	cmp_func_t cmp;
	size_t cant;
	size_t cap;
};

heap_t *heap_crear(cmp_func_t cmp) {
	heap_t *heap = malloc(sizeof(heap_t));
	if (!heap) return NULL;

	void **arreglo = malloc(sizeof(void*) * TAM_INICIAL);
	if (!arreglo) {
		free(heap);
		return NULL;
	}

	for (int i = 0; i < TAM_INICIAL; i++) {
		arreglo[i] = NULL;
	}

	heap->arreglo = arreglo;
	heap->cmp = cmp;
	heap->cap = TAM_INICIAL;
	heap->cant = 0;

	return heap;
}

size_t heap_cantidad(const heap_t *heap) {
	return heap->cant;
}

bool heap_esta_vacio(const heap_t *heap) {
	return heap->cant == 0;
}

void *heap_ver_max(const heap_t *heap) {
	return heap->arreglo[0];
}

void upheap(heap_t *heap, size_t posicion) {
	void **arreglo = heap->arreglo;
	void *elem_act = arreglo[posicion];
	cmp_func_t cmp = heap->cmp;
	if (posicion == 0) return;

	size_t pos_padre = (posicion - 1) / 2;
	void *padre = arreglo[pos_padre];

	if (cmp(elem_act,padre) > 0) {
		arreglo[posicion] = padre;
		arreglo[pos_padre] = elem_act;
		upheap(heap, pos_padre);
	}

	return;
}

void downheap(heap_t *heap, size_t posicion) {
	if (heap->cant == 1 || posicion >	(heap->cant / 2) - 1) return;
	void **arreglo = heap->arreglo;
	size_t pos_act = posicion;
	void *elem_act = arreglo[posicion];
	cmp_func_t cmp = heap->cmp;

	size_t pos_hijo_izq = posicion * 2 + 1;
	void *hijo_izq = arreglo[pos_hijo_izq];
	if (hijo_izq) {
		if(cmp(arreglo[pos_act], hijo_izq) < 0) pos_act = pos_hijo_izq;
	}

	size_t pos_hijo_der = posicion * 2 + 2;
	if(pos_hijo_der < heap->cant){
		void* hijo_der = arreglo[pos_hijo_der];
		if (hijo_der) {
			if(cmp(arreglo[pos_act], hijo_der) < 0) pos_act = pos_hijo_der;
		}
	}

	if (posicion == pos_act) return;
	arreglo[posicion] = arreglo[pos_act];
	arreglo[pos_act] = elem_act;
	downheap(heap, pos_act);
}

bool redimension(heap_t* heap, bool encolar){
	void** datos_nuevos = NULL;
	size_t cap_nueva = 0;
	if(encolar && heap->cant == heap->cap) cap_nueva = heap->cap * CONST_REDIMENSION;
	if(!encolar && heap->cant == (heap->cap / CONST_REDIMENSION) && heap->cap > TAM_INICIAL) cap_nueva = heap->cap / CONST_REDIMENSION;
	if(cap_nueva > 0){
		datos_nuevos = realloc(heap->arreglo, cap_nueva * sizeof(void*));
		if(!datos_nuevos) return false;
		heap->arreglo = datos_nuevos;
		heap->cap = cap_nueva;
	}
	return true;
}

bool heap_encolar(heap_t *heap, void *elem) {
	if (!elem) return false;

	if (!redimension(heap, true)) return false;

	void **arreglo = heap->arreglo;

	arreglo[heap->cant] = elem;

	upheap(heap, heap->cant);

	heap->cant ++;
	return true;
}

void *desencolar(heap_t *heap){
	void **arreglo = heap->arreglo;
	void *elem = arreglo[0];

	arreglo[0] = arreglo[heap->cant - 1];
	arreglo[heap->cant - 1] = NULL;

	downheap(heap, 0);

	heap->cant --;
	return elem;
}

void *heap_desencolar(heap_t *heap) {
	if (heap_esta_vacio(heap)) return NULL;

	if (!redimension(heap, false)) return false;

	return desencolar(heap);
}

void heapify(heap_t* heap) {
	if (heap->cant <= 1) return;

	int cantidad = (int) heap->cant;
	for (int i = (cantidad / 2 - 1); i >= 0; i--) {
		downheap(heap, i);
	}
}

heap_t *heap_crear_arr(void *arreglo[], size_t n, cmp_func_t cmp) {
	heap_t *heap = malloc(sizeof(heap_t));
	if (!heap) return NULL;

	heap->arreglo = arreglo;
	heap->cap = n;
	heap->cmp = cmp;
	heap->cant = n;

	heapify(heap);

	return heap;
}

void heap_destruir(heap_t *heap, void destruir_elemento(void *)) {
	if (destruir_elemento) {
		for (size_t i = 0; i < heap->cant; i++) {
			destruir_elemento(heap->arreglo[i]);
		}
	}

	free(heap->arreglo);
	free(heap);
}

void heap_sort(void *elementos[], size_t cant, cmp_func_t cmp) {
	heap_t* heap = heap_crear_arr(elementos, cant, cmp);
	if(!heap) return;
	while(heap_cantidad(heap) > 0){
		elementos[heap_cantidad(heap) - 1] = desencolar(heap);
	}
	free(heap);
}