#include "strutil.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char *strdup (const char *s) {
    char *d = malloc (strlen (s) + 1);   // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy (d,s);                        // Copy the characters
    return d;                            // Return the new string
}

char* copystr(const char* src,size_t len){
  char* strv = malloc(sizeof(char*) * len);
  if(strv == NULL){
    return NULL;
  }
  memcpy(strv, src, len - 1);
  strv[len - 1] = '\0';
  return strv;
}

char** split(const char* str, char sep){
  size_t index = 0;
  size_t apariciones = 0;
  while(str[index] != '\0'){
    if(str[index] == sep) apariciones++;
    index++;
  }

  char **strv = malloc(sizeof(char*) * (apariciones + 2));
  if(!strv) return NULL;

  // Inicializo todos los vectores
  for (int i = 0; i < apariciones + 2; ++i){
    strv[i] = NULL;
  }

  // Si la cadena contiene caracteres
  if(str[0] != '\0'){
    size_t ultimo = 0;
    size_t contador = 0;
    index = 0;
    while(str[index] != '\0'){
      if (str[index] == sep){
        strv[contador] = copystr(str + ultimo, index - ultimo + 1);
        ultimo = index + 1;
        contador ++;
      }
      if (str[index + 1] == '\0'){
        strv[apariciones] = copystr(str + ultimo, index - ultimo + 2);
      }
      index++;
    }
  } else {
    strv[0] = strdup("");
  }
  return strv;
}

void free_strv(char* strv[]){
  size_t index = 0;
  while(strv[index] != NULL){
    free(strv[index]);
    index ++;
  }
  free(strv);
}

// Calcula el largo del vector devuelto por el split.
// PRE: el arreglo fue generado por el split y cumple con las condiciones del POST 
// del mismo.
// POST: devuelve la cantidad de elementos sin el NULL del vector.
size_t largo(char** str){
  size_t index = 0;
  for(; str[index] != NULL; index++);
  return index;
}

// Remove el trailing del final '\n' en caso de tenerlo. Esta funcion modifica la
// cadena que se le pasa por parametro.
void remove_trailing(char* string){
  size_t ln = strlen(string) - 1;
  if (*string && string[ln] == '\n') string[ln] = '\0';
}
