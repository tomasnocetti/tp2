#define _POSIX_C_SOURCE 200809L
#include "hash.h"
#include "lista.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define COF_INI 50
#define MAX_FC 0.7
#define MIN_FC 0.3
#define REDIMENSION 2


struct hash {
	lista_t** tabla;
	size_t capacidad;
	size_t cantidad;
	hash_destruir_dato_t destruir_dato;
};

typedef struct campo { 
	char* clave;
	void* valor;
} campo_t;

struct hash_iter{
	const hash_t* hash;
	lista_iter_t* lista_iter;
	size_t actual;
};

unsigned long hashing(const char *str, size_t cantidad) {
  unsigned long hash = 5381;
  int c;
  while ((c = *str++)){
      hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  }

  return hash % cantidad;
}

hash_t *hash_crear(hash_destruir_dato_t destruir_dato) {

	hash_t* hash = malloc(sizeof(hash_t));

	if (hash == NULL) {
		return NULL;
	}

	hash->tabla = malloc(sizeof(lista_t*) * COF_INI);

	if(hash->tabla == NULL) {
		free(hash);
		return NULL;
	}

	for(int iter = 0; iter < COF_INI; iter++) { 
		hash->tabla[iter] = NULL; 
	}

	hash->capacidad = COF_INI;
	hash->cantidad = 0;
	hash->destruir_dato = destruir_dato;
	return hash;
}

campo_t* campo_crear(const char* clave, void* valor) {
	campo_t* campo = malloc(sizeof(campo_t));

	if (campo == NULL) {
		return NULL;
	}

	campo->clave = strdup(clave);
	campo->valor = valor;
	return campo;
}


void destruir_campo(campo_t* campo, hash_destruir_dato_t destruir_dato, bool datos){
	if(destruir_dato && datos) destruir_dato(campo->valor);
	free(campo->clave);
	free(campo);
}

void tabla_destruir(hash_t* hash, bool datos) {
	for(size_t i = 0; i < hash->capacidad; i++){
		if(hash->tabla[i] == NULL) continue;
		lista_iter_t* iter = lista_iter_crear(hash->tabla[i]);
		size_t index = 0;
		campo_t* cam = lista_iter_borrar(iter);
		while(cam != NULL){
			destruir_campo(cam, hash->destruir_dato, datos);
			index++;
			cam = lista_iter_borrar(iter);
		}
		lista_iter_destruir(iter);
		lista_destruir(hash->tabla[i], NULL);
	}

	free(hash->tabla);
}

void hash_destruir(hash_t *hash){
	tabla_destruir(hash, true);
	free(hash);
}


size_t hash_cantidad(const hash_t *hash){
	return hash->cantidad;
}

bool hash_pertenece(const hash_t *hash, const char *clave) {
	size_t  posicion = hashing(clave, hash->capacidad);

	lista_t* lista = hash->tabla[posicion];
	if(!lista) return false;
	lista_iter_t* iter = lista_iter_crear(lista);

	for(;!lista_iter_al_final(iter); lista_iter_avanzar(iter)) {
		campo_t* campo = lista_iter_ver_actual(iter);
		if (strcmp(campo->clave, clave) == 0) {
			lista_iter_destruir(iter);
			return true;
		}
	}

	lista_iter_destruir(iter);
	return false;
}

void *hash_obtener(const hash_t *hash, const char *clave) {
	size_t posicion = hashing(clave, hash->capacidad);

	lista_t* lista = hash->tabla[posicion];
	if(!lista) return NULL;
	lista_iter_t* iter = lista_iter_crear(lista);

	for(;!lista_iter_al_final(iter); lista_iter_avanzar(iter)) {
		campo_t* campo = lista_iter_ver_actual(iter);
		if (strcmp(campo->clave, clave) == 0) {
			lista_iter_destruir(iter);
			return campo->valor;
		}
	}
	lista_iter_destruir(iter);
	return NULL;
}




hash_iter_t *hash_iter_crear(const hash_t* hash){
	hash_iter_t* iter = malloc(sizeof(hash_iter_t));
	if(!iter) return NULL;

	iter->hash = hash;

	for(size_t i = 0; i < hash->capacidad; i++){
		iter->actual = i;
		if(hash->tabla[i] != NULL){
			iter->lista_iter = lista_iter_crear(hash->tabla[i]);

			if(!iter->lista_iter) {
				free(iter);
				return NULL;
			}
			return iter;
		}
	}
	iter->actual ++;
	return iter;
}

bool hash_iter_al_final(const hash_iter_t *iter){
	return iter->actual == iter->hash->capacidad;
}

bool hash_iter_avanzar(hash_iter_t *iter){
	if(hash_iter_al_final(iter)) return false;

	if(lista_iter_avanzar(iter->lista_iter)){
		if(!lista_iter_al_final(iter->lista_iter)){
			return true;
		}
	}

	lista_iter_destruir(iter->lista_iter);

	for(size_t i = iter->actual + 1; i < iter->hash->capacidad; i++){
		iter->actual = i;
		if(iter->hash->tabla[i] != NULL && !lista_esta_vacia(iter->hash->tabla[i])){
			iter->lista_iter = lista_iter_crear(iter->hash->tabla[i]);
			return !(iter->lista_iter == NULL);
		}
	}
	iter->actual++;
	return false;
}

const char *hash_iter_ver_actual(const hash_iter_t *iter){
	if(iter->actual == iter->hash->capacidad) return NULL;
	if (lista_esta_vacia(iter->hash->tabla[iter->actual])) return NULL;
	campo_t* campo = lista_iter_ver_actual(iter->lista_iter);
	return campo->clave;
}

void hash_iter_destruir(hash_iter_t* iter){
	free(iter);
}

bool chequear_redimension(hash_t* hash){
	double redim = (double) hash->cantidad / (double) hash->capacidad;
	return (redim < MIN_FC && hash->capacidad / REDIMENSION >= COF_INI) || redim > MAX_FC;
}

bool redimensionar(hash_t* hash, size_t tamanio) {
	lista_t** tabla_nueva = malloc(sizeof(lista_t*) * tamanio);
	if (!tabla_nueva) return false;

	for(int iter = 0; iter < tamanio; iter++) {
		tabla_nueva[iter] = NULL;
	}

	hash_iter_t* iter = hash_iter_crear(hash);

	for(;!hash_iter_al_final(iter); hash_iter_avanzar(iter)) {
		const char* clave = hash_iter_ver_actual(iter);
		if (!clave) continue;
		void* valor = hash_obtener(hash, clave);
		campo_t* campo = campo_crear(clave, valor);
		if (!campo) return false;

		size_t  posicion = hashing(clave, tamanio);

		if(!tabla_nueva[posicion]) {
			tabla_nueva[posicion] = lista_crear();
			if(!tabla_nueva[posicion]) return false;
		}

		lista_t* lista = tabla_nueva[posicion];
		lista_insertar_ultimo(lista, campo);
	}

	hash_iter_destruir(iter);

	tabla_destruir(hash, false);
	hash->tabla = tabla_nueva;
	hash->capacidad = tamanio;
	return true;
}

void *hash_borrar(hash_t *hash, const char *clave) {

	size_t posicion = hashing(clave, hash->capacidad);

	lista_t* lista = hash->tabla[posicion];
	if(!lista) return NULL;
	lista_iter_t* iter = lista_iter_crear(lista);

	for(;!lista_iter_al_final(iter); lista_iter_avanzar(iter)) {
		campo_t* campo = lista_iter_ver_actual(iter);
		if (strcmp(campo->clave, clave) == 0) {
			void* valor = campo->valor;
			free(campo->clave);
			free(campo);
			lista_iter_borrar(iter);
			lista_iter_destruir(iter);
			hash->cantidad--;
			if (chequear_redimension(hash)) redimensionar(hash, hash->capacidad / REDIMENSION);
			return valor;
		}
	}
	lista_iter_destruir(iter);
	return NULL;
}


bool hash_guardar(hash_t *hash, const char *clave, void *dato) {

	size_t posicion = hashing(clave, hash->capacidad);
	lista_t** tabla = hash->tabla;

	if(!tabla[posicion]) {
		tabla[posicion] = lista_crear();
		if(!tabla[posicion]) return false;
	}

	if (chequear_redimension(hash)){
		if (!redimensionar(hash, hash->capacidad * REDIMENSION)) return false;
	}

	posicion = hashing(clave, hash->capacidad);
	tabla = hash->tabla;

	if (!tabla[posicion]) {
		tabla[posicion] = lista_crear();
		if(!tabla[posicion]) return false;
	}

	lista_t* lista = tabla[posicion];

	if (hash_pertenece(hash,clave)) {
		lista_iter_t* iter = lista_iter_crear(lista);
		for(;!lista_iter_al_final(iter); lista_iter_avanzar(iter)) {
			campo_t* campo = lista_iter_ver_actual(iter);
			if (strcmp(campo->clave, clave) == 0) {
				void* valor = campo->valor;
				campo->valor = dato;
			if(hash->destruir_dato){
				hash->destruir_dato(valor);
			}
		lista_iter_destruir(iter);
		return true;
		}
	}
	}

	campo_t* campo = campo_crear(clave, dato);
	if(!campo) return false;

	hash->cantidad++;
	return lista_insertar_ultimo(lista, campo);
}
