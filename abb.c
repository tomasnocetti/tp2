#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "abb.h"
#include "pila.h"

typedef struct abb_nodo {
	struct abb_nodo* izq;
	struct abb_nodo* der;
	struct abb_nodo* padre;
	char* clave;
	void* dato;
} abb_nodo_t;

struct abb {
	abb_nodo_t* raiz;
	abb_comparar_clave_t cmp; 
	abb_destruir_dato_t destruir_dato;
	size_t cant;
};

struct abb_iter {
	pila_t* pila;
	const abb_t* arbol;
};

abb_nodo_t* abb_nodo_crear (const char* clave, void* dato) {
	abb_nodo_t* nodo = malloc(sizeof(abb_nodo_t));
	nodo->clave = strdup(clave);
	nodo->dato = dato;
	nodo->izq = NULL;
	nodo->der = NULL;
	nodo->padre = NULL;
	return nodo;
}

abb_t* abb_crear(abb_comparar_clave_t cmp, abb_destruir_dato_t destruir_dato) {
	abb_t* arbol = malloc(sizeof(abb_t));

	if (arbol == NULL) {
		return NULL;
	}

	arbol->raiz = NULL;
	arbol->cant = 0;
	arbol->cmp = cmp;
	arbol->destruir_dato = destruir_dato;
	return arbol;
}

abb_nodo_t* abb_nodo_buscar(abb_nodo_t* nodo, const char* clave, abb_comparar_clave_t cmp) {
	if (!nodo)	return NULL;
	if (cmp(nodo->clave,clave) == 0) {
		return nodo;
	} else if (cmp(nodo->clave,clave) > 0){
		if (nodo->izq != NULL) return abb_nodo_buscar(nodo->izq, clave, cmp);
	} else {
		if (nodo->der != NULL) return abb_nodo_buscar(nodo->der, clave, cmp);
	}
	return NULL;
}

void *abb_obtener(const abb_t *arbol, const char *clave) {
	abb_nodo_t* nodo = abb_nodo_buscar(arbol->raiz, clave, arbol->cmp);
	if (nodo == NULL) return NULL;
	return nodo->dato ;
}

bool abb_pertenece(const abb_t *arbol, const char *clave) {
	abb_nodo_t* nodo = abb_nodo_buscar(arbol->raiz, clave, arbol->cmp);
	if (!nodo) return false;
	return true;
}

void destruir_contenido_nodo(abb_nodo_t* nodo, abb_destruir_dato_t destruir_dato){
	if(destruir_dato){
		destruir_dato(nodo->dato);
	}
	free(nodo->clave);
}

void abb_nodo_destruir(abb_nodo_t* nodo,abb_destruir_dato_t destruir_dato) {
	destruir_contenido_nodo(nodo, destruir_dato);
	free(nodo);
}

bool abb_nodo_insertar(abb_nodo_t* nodo,const char* clave, void* dato,abb_comparar_clave_t cmp,abb_destruir_dato_t destruir_dato ) {
	if (cmp(nodo->clave,clave) == 0) {
		destruir_contenido_nodo(nodo, destruir_dato);
		nodo->clave = strdup(clave);
		nodo->dato = dato;
		return true;
	} else if (cmp(nodo->clave,clave) > 0){
		if (nodo->izq != NULL){
			return abb_nodo_insertar(nodo->izq, clave, dato, cmp, destruir_dato);
		}
		nodo->izq = abb_nodo_crear(clave,dato);
		nodo->izq->padre = nodo;
	} else {
		if (nodo->der != NULL){
			return abb_nodo_insertar(nodo->der, clave, dato, cmp, destruir_dato);
		}
		nodo->der = abb_nodo_crear(clave,dato);
		nodo->der->padre = nodo;
	}
	return true;
}

bool abb_guardar(abb_t *arbol, const char *clave, void *dato) {
	if (arbol->raiz == NULL) {
		arbol->raiz = abb_nodo_crear(clave, dato);
		if (!arbol->raiz) return false;
		arbol->cant ++;
		return true;
	}
	if (!abb_pertenece(arbol,clave))  arbol->cant ++;

	return abb_nodo_insertar(arbol->raiz, clave, dato, arbol->cmp, arbol->destruir_dato);
}

abb_nodo_t* abb_nodo_minimo(abb_nodo_t* nodo) {
	abb_nodo_t* act = nodo;
	while (act->izq != NULL) {
		act = act->izq;
	}
	return act;
}

abb_nodo_t* abb_nodo_sucesor(abb_nodo_t* nodo) {
	if (nodo->der != NULL)  return abb_nodo_minimo(nodo->der);

	abb_nodo_t* act = nodo;
	while (act->padre != NULL && act->padre->der == act) {
		act = act->padre;
	}
	return act->padre;
}

void asignar_referencias_nodo(abb_t *arbol, abb_nodo_t* nodo, abb_nodo_t* nodo_asignar){
	if (arbol->raiz == nodo) {
		arbol->raiz = nodo_asignar;
	} else if (arbol->cmp(nodo->clave,nodo->padre->clave) < 0) {
		nodo->padre->izq = nodo_asignar;
	} else {
		nodo->padre->der = nodo_asignar;
	}
}

void *abb_borrar(abb_t *arbol, const char *clave) {
	if (!abb_pertenece(arbol,clave)) {
		return NULL;
	}
	abb_nodo_t* nodo = abb_nodo_buscar(arbol->raiz, clave, arbol->cmp);
	void* dato = nodo->dato;

	if (!nodo->izq && !nodo->der) {
		asignar_referencias_nodo(arbol, nodo, NULL);
		abb_nodo_destruir(nodo, arbol->destruir_dato);
	} else if (!nodo->izq && nodo->der) {
		asignar_referencias_nodo(arbol, nodo, nodo->der);
		nodo->der->padre = nodo->padre;
		abb_nodo_destruir(nodo, arbol->destruir_dato);
	} else if (nodo->izq && !nodo->der) {
		asignar_referencias_nodo(arbol, nodo, nodo->izq);
		nodo->izq->padre = nodo->padre;
		abb_nodo_destruir(nodo, arbol->destruir_dato);
	} else {
		abb_nodo_t* reemplazo = abb_nodo_sucesor(nodo);
		destruir_contenido_nodo(nodo, arbol->destruir_dato);

		nodo->clave = reemplazo->clave;
		nodo->dato = reemplazo->dato;
		if (reemplazo == nodo->der) {
			nodo->der = reemplazo->der;
		} else {
			reemplazo->padre->izq = reemplazo->der;
		}
		if (reemplazo->der) reemplazo->der->padre = reemplazo->padre;

		free(reemplazo);
	}
	arbol->cant --;
	return dato;
}

size_t abb_cantidad(abb_t *arbol) {
	return arbol->cant;
}

bool abb_iterar_in_order(abb_nodo_t* nodo, bool visitar(const char *, void *, void *), void *extra) {
	if (nodo == NULL) return true;
	if (!abb_iterar_in_order(nodo->izq,visitar,extra)) return false;
	if (!visitar(nodo->clave,nodo->dato,extra)) return false;
	return abb_iterar_in_order(nodo->der,visitar,extra);
}

void abb_in_order(abb_t *arbol, bool visitar(const char *, void *, void *), void *extra) {
	abb_iterar_in_order(arbol->raiz,visitar, extra);
}

void abb_iterar_pos_order(abb_nodo_t* nodo, abb_destruir_dato_t destruir_dato ) {
	if (nodo == NULL) return;
	abb_iterar_pos_order(nodo->izq, destruir_dato);
	abb_iterar_pos_order(nodo->der, destruir_dato);
	abb_nodo_destruir(nodo,destruir_dato);
}

void abb_destruir(abb_t *arbol) {
	abb_iterar_pos_order(arbol->raiz,arbol->destruir_dato);
	free(arbol);
}

abb_iter_t *abb_iter_in_crear(const abb_t *arbol) {
	abb_iter_t* iter = malloc(sizeof(abb_iter_t));

	if (!iter) return NULL;

	pila_t* pila = pila_crear();

	abb_nodo_t* act = arbol->raiz;
	while (act) {
		pila_apilar(pila, act);
		act = act->izq;
	}

	iter->pila = pila;
	iter->arbol = arbol;
	return iter;
}

bool abb_iter_in_avanzar(abb_iter_t *iter) {
	if (abb_iter_in_al_final(iter)) return false;
	abb_nodo_t* anterior = pila_desapilar(iter->pila);

	if (!anterior->der) return true;
	abb_nodo_t* act = anterior->der;

	while (act) {
		pila_apilar(iter->pila, act);
		act = act->izq;
	}
	return true;
}

const char *abb_iter_in_ver_actual(const abb_iter_t *iter) {
	abb_nodo_t* actual = pila_ver_tope(iter->pila);
	if(!actual) return NULL;
	return actual->clave;
}

bool abb_iter_in_al_final(const abb_iter_t *iter) {
	return pila_esta_vacia(iter->pila);
}

void abb_iter_in_destruir(abb_iter_t* iter) {
	pila_destruir(iter->pila);
	free(iter);
}

void abb_iterar_in_order_claves(abb_nodo_t* nodo, abb_comparar_clave_t cmp, void visitar(const char *), char* desde, char* hasta) {
	if (nodo == NULL) return;

  if ( (!desde || cmp(nodo->clave, desde) > 0) && nodo->izq != NULL){
		abb_iterar_in_order_claves(nodo->izq, cmp, visitar, desde, hasta);
	}
	if ( (!desde || cmp(nodo->clave, desde) >= 0) && (!hasta || cmp(nodo->clave, hasta) <= 0) ) {
		visitar(nodo->clave);
	}
	if ( (!hasta || cmp(nodo->clave, hasta) < 0) && nodo->der != NULL) {
	  abb_iterar_in_order_claves(nodo->der, cmp, visitar, desde, hasta);
	}
}

void abb_claves_in_order(abb_t *arbol, void visitar(const char*), char* desde, char* hasta) {
	abb_iterar_in_order_claves(arbol->raiz, arbol->cmp, visitar, desde, hasta);
}