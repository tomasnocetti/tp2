#define _POSIX_C_SOURCE 200809L //getline
#include <stdbool.h>  /* bool */
#include <stddef.h>	  /* size_t */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dos.h"
#include "strutil.h"
#include "timeutils.h"
#include "abb.h"
#include "hash.h"
#include "heap.h"
#include "lista.h"

#define POSICION_IP 0
#define POSICION_TIMESTAMP 1
#define POSICION_METODO 2
#define POSICION_URL 3
#define DELT_TIEMPO 2
#define POS_INI_ITER2 5
#define SEPARADOR_LOG '\t'
#define DOS_PRINT "DoS:"


/* Funcion que devuelve 1 si ip1 es mayor ip2, 0 si son iguales, -1 si ip1 es menor a ip2*/
int comparar_ips(const char* ip1, const char* ip2) {
  if(!ip2) return 1;
  if(!ip1) return -1;

  char** ip1_s = split(ip1, '.');
	char** ip2_s = split(ip2, '.');
	int comparar = 0;

	for(size_t i = 0; i < 4; i++) {
	
  	if (atoi(ip1_s[i]) > atoi(ip2_s[i])){
			comparar = 1;
			break;
		}
	
  	if (atoi(ip1_s[i]) < atoi(ip2_s[i])) {
			comparar = -1;
			break;
		}
	}
	
  free_strv(ip1_s);
	free_strv(ip2_s);
	return comparar;
}

int cmp_recursos(const void* a, const void* b){
  const recurso_t* a_r = a;
  const recurso_t* b_r = b;
  return (int)b_r->visitas - (int)a_r->visitas;
}

void liberar_recurso(void* a){
  recurso_t* recurso = a;
  free(recurso->url);
  free(recurso);
}

sistema_t* iniciar_sistema() {
	
  sistema_t* sistema = malloc(sizeof(sistema_t));
	
  if (!sistema) return NULL;

	sistema->ips = abb_crear(comparar_ips, free);
  sistema->recursos = hash_crear(liberar_recurso);

	if(!sistema->ips || !sistema->recursos) {
		terminar_sistema(sistema);
		return NULL;
	}

	return sistema;
}

void terminar_sistema(sistema_t* sistema) {
	abb_destruir(sistema->ips);
	hash_destruir(sistema->recursos);
	free(sistema);
}

bool agregar_visita(hash_t* recursos, const char* url) {
  recurso_t* recurso;
  
  if(hash_pertenece(recursos, url)) {
    recurso = hash_obtener(recursos, url);
    recurso->visitas++;
    return true;
  }

	recurso = malloc(sizeof(recurso_t));
	recurso->url = strdup(url);
  recurso->visitas = 1;

	return hash_guardar(recursos, url, recurso);
}


void ver_mas_visitados(sistema_t* sistema, size_t cantidad, visitados_iter_t iterar){
  if(cantidad < 1) return;

  heap_t* heap = heap_crear(cmp_recursos);

  hash_iter_t* iter = hash_iter_crear(sistema->recursos);

  recurso_t* recurso;

  // Utilizo un heap de minimos (alterando la funcion de comparacion);
  while(!hash_iter_al_final(iter)){
    const char* clave = hash_iter_ver_actual(iter);
    recurso = hash_obtener(sistema->recursos, clave);

    if(heap_cantidad(heap) < cantidad) {
      heap_encolar(heap, recurso);
    } else if(cmp_recursos(heap_ver_max(heap), recurso) > 0){
      heap_desencolar(heap);
      heap_encolar(heap, recurso);
    }
    hash_iter_avanzar(iter);
  }

  cantidad = heap_cantidad(heap);
  recurso_t** array_recursos = malloc(sizeof(recurso_t*) * cantidad);

  int i;
  // Ordeno en un vector como lo necesito.
  for(i = (int) cantidad - 1; i >= 0; i--){
    array_recursos[i] = heap_desencolar(heap);
  }

  for(i = 0; i < cantidad; i++){
    recurso = array_recursos[i];
    iterar(recurso->url, recurso->visitas);
  }

  free(array_recursos);
  hash_iter_destruir(iter);
  heap_destruir(heap, NULL);
}

void ver_visitantes(sistema_t* sistema, visitantes_iter_t iterar, char* desde, char* hasta){
  abb_claves_in_order(sistema->ips, iterar, desde, hasta);
}


void guardar_tiempos(hash_t* hash, const char* ip, const char* tiempo){
  
  lista_t* lista;
  
  if(!hash_pertenece(hash, ip)){
    lista = lista_crear();
    hash_guardar(hash, ip, lista);
  } else {
    lista = hash_obtener(hash, ip);
  }
  
  time_t* tiempo_s = malloc(sizeof(time_t));
  *tiempo_s = iso8601_to_time(tiempo);
  lista_insertar_ultimo(lista, tiempo_s);
}

void agregar_ip(sistema_t* sistema, const char* ip, const char* tiempo, hash_t* hash_aux) {
  abb_t* abb = sistema->ips;

  guardar_tiempos(hash_aux, ip, tiempo);

  abb_guardar(abb, ip, NULL);
}

bool inicio_iter_2 (lista_iter_t* iter) {
  for (int i = 1; i < POS_INI_ITER2; i++){
    if (!lista_iter_avanzar(iter)) return false;
  }
  
  return true;
}

void detectar_DoS(lista_iter_t* lista_iter1, lista_iter_t* lista_iter2, const char* ip) {
  while(!lista_iter_al_final(lista_iter2)){
    time_t* tiempo1 = lista_iter_ver_actual(lista_iter1);
    
    time_t* tiempo2 = lista_iter_ver_actual(lista_iter2);
    
    if(((int) difftime(*tiempo2, *tiempo1)) < DELT_TIEMPO) {
      fprintf(stdout, "%s %s\n", DOS_PRINT, ip);
      return;
    }
    
    lista_iter_avanzar(lista_iter1);
    lista_iter_avanzar(lista_iter2);
  }
}

void destruir_lista(lista_iter_t* lista_iter1, lista_iter_t* lista_iter2, lista_t* lista) {
    lista_iter_destruir(lista_iter1);
    lista_iter_destruir(lista_iter2);
    lista_destruir(lista, free);
}

void imprimir_dos(hash_t* hash, sistema_t* sistema){
  abb_iter_t* iter = abb_iter_in_crear(sistema->ips);
  
  while(!abb_iter_in_al_final(iter)){
    
    const char* actual = abb_iter_in_ver_actual(iter);
    
    if (!hash_pertenece(hash,actual)) {
      abb_iter_in_avanzar(iter);
      continue;
    }
    
    lista_t* lista = (lista_t*) hash_obtener(hash, actual);
    
    lista_iter_t* lista_iter1 = lista_iter_crear(lista);
    lista_iter_t* lista_iter2 = lista_iter_crear(lista);

    if (inicio_iter_2(lista_iter2)) {
      detectar_DoS(lista_iter1, lista_iter2,actual);
    }
    
    destruir_lista(lista_iter1, lista_iter2, lista);
    abb_iter_in_avanzar(iter);
    
  }

  abb_iter_in_destruir(iter);
}

bool procesar_archivo(sistema_t* sistema, char* nombre_archivo){
  char* linea = NULL; size_t capacidad = 0; ssize_t leidos; //combo getline

  FILE* archivo = fopen(nombre_archivo,"r");
  if (!archivo) return false;

  hash_t* hash_aux = hash_crear(NULL);
  bool ok = true;

  while((leidos = getline(&linea,&capacidad,archivo)) > 0){
    size_t ln = strlen(linea) - 1;
    if (*linea && linea[ln] == '\n') linea[ln] = '\0';

    char** datos = split(linea, SEPARADOR_LOG);
    if(!datos) {
      ok = false;
      break;
    }

    if(!agregar_visita(sistema->recursos, datos[POSICION_URL])) {
      ok = false;
      break;
    }

    agregar_ip(sistema, datos[POSICION_IP], datos[POSICION_TIMESTAMP], hash_aux);

    free_strv(datos);
  }

  if (ok) imprimir_dos(hash_aux,sistema);
  
  free(linea);
  fclose(archivo);
  hash_destruir(hash_aux);
  
  return ok;
}