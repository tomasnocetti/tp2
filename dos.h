#ifndef DOS_H
#define DOS_H

#include <stdbool.h>  /* bool */
#include <stddef.h>   /* size_t */
#include <time.h>
#include "abb.h"
#include "heap.h"
#include "hash.h"


typedef struct sistema{
  abb_t* ips;
  hash_t* recursos;
} sistema_t;

typedef struct recurso{
	char* url;
	size_t visitas;
} recurso_t;

typedef void (*visitantes_iter_t) (const char*);
typedef void (*visitados_iter_t) (const char*, size_t);

/* El metodo iniciar sistema, creara las estructuras donde se guardaran los datos, recaudados.
Post: un struct del tipo sistema con las estructuras fue creado. */
sistema_t* iniciar_sistema(void);

/* Liberara la memoria requerida por el sistema.
Pre: el sistema fue creado.
Post: la memoria requerida para el almacenaje de la informacion fue liberada.*/
void terminar_sistema(sistema_t* sistema);

/* Procesa el archivo y detecta posibles ataques de DoS, ademas de almacenar la informacion del uso de los recursos y 
de los ips que ingresaron a la pagina. Devuelve True si se pudo procesar de forma correcta, y false en caso contrario.
Pre: El sistema fue creado.
Post: Se proceso el archivo y se guardo toda la informacion, y se devolvio true en caso de que no haya habido un error, 
de otra forma se devolvio false.   */
bool procesar_archivo(sistema_t* sistema, char* archivo);

/* Procesa la informacion almacenada y a traves de una funcion pasada por parametro, imprime por pantalla
los visitantes que pertenecen a un conjunto recibido por parametro.
Pre: el sistema fue creado.
Post: Se imprimieron los visitantes en el conjuntos recibido.*/
void ver_visitantes(sistema_t* sistema, visitantes_iter_t iterar, char* ip_desde, char* ip_hasta);


/* Procesa la informacion almacenada y a traves de una funcion pasada por parametro, imprime por pantalla
los sitios mas visitados.
Pre: el sistema fue creado.
Post: Se imprimieron los sitios mas visitados.*/
void ver_mas_visitados(sistema_t* sistema, size_t cantidad, visitados_iter_t iterar);

#endif // DOS_H