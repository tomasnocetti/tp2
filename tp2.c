#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 

#include "strutil.h"
#include "dos.h"
#include "testing.h"


#define AGREGAR_ARCHIVO "agregar_archivo"
#define VER_VISITANTES "ver_visitantes"
#define VER_MAS_VISITADOS "ver_mas_visitados"
#define ERROR_COMANDO "Error en comando"
#define VISTANTES "Visitantes:"
#define SITIOS_VISITADOS "Sitios mas visitados:"
#define TAB '\t'



/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/
#define OK "OK\n"

void imprimir(const char* ip) {
  printf("%c%s\n", TAB, ip);
}

void visitar(const char* url, size_t cantidad) {
  printf("%c%s - %lu\n", TAB, url, cantidad);
}

void pruebas () {
  pruebas_sistema();
}

int main(){
  // pruebas();
  sistema_t* sistema = iniciar_sistema();

  if(!sistema) return 1;

  char* linea = NULL; size_t capacidad = 0; ssize_t leidos; //combo getline
  int err = 0;

  while((leidos = getline(&linea,&capacidad,stdin)) > 0){
    remove_trailing(linea);
    char** datos = split(linea, ' ');

    if(!datos) return 1;

    size_t parametros = largo(datos);

    if(parametros == 0){
      free_strv(datos);
      continue;
    }

    char* comando = datos[0];
    if(strcmp(AGREGAR_ARCHIVO, comando) == 0){
      if(parametros != 2) err = true;
      else {
        char* archivo = datos[1];
        if (!procesar_archivo(sistema,archivo)) {
          err = true;
        } else {
          printf(OK);
        }
      }
    } else if(strcmp(VER_VISITANTES, comando) == 0){
      if(parametros != 3) err = true;
      else {
        char* desde = datos[1];
        char* hasta = datos[2];
        printf("%s\n", VISTANTES);
        ver_visitantes(sistema, imprimir, desde, hasta);
        printf(OK);
      }
    } else if(strcmp(VER_MAS_VISITADOS, comando) == 0){
      if(parametros != 2) err = true;
      else {
        size_t visitados = atoi(datos[1]);
        printf("%s\n", SITIOS_VISITADOS);
        ver_mas_visitados(sistema, visitados, visitar);
        printf(OK);
      }
    } else {
      err = true;
    }

    if(err){
      fprintf(stderr, "%s %s\n",ERROR_COMANDO, comando);
      free_strv(datos);
      break;
    }

    free_strv(datos);
  }
  free(linea);
  terminar_sistema(sistema);
  return 0;
}

