#ifndef LISTA_H
#define LISTA_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


/* *****************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

/* Se trata de una lista que contiene datos de tipo void*
 * (punteros genéricos).  La lista en sí está definida en el .c.  */

struct lista;  // Definición completa en lista.c.
typedef struct lista lista_t;
struct lista_iter;  // Definición completa en lista.c.
typedef struct lista_iter lista_iter_t;


/* *****************************************************************
 *                    PRIMITIVAS DE LA LISTA
 * *****************************************************************/

// Crea una lista.
// Post: devuelve una nueva lista vacía.
lista_t *lista_crear(void);

// Destruye la lista.
// Pre: la lista fue creada. Destruir_dato es una función capaz de destruir
// los datos de la cola, o NULL en caso de que no se la utilice.
// Post: Si se recibe la función destruir_dato por parámetro,
// para cada uno de los elementos de la lista llama a destruir_dato.
void lista_destruir(lista_t *lista, void destruir_dato(void *));

// Devuelve verdadero o falso, según si la lista tiene o no elementos guardados.
// Pre: la lista fue creada.
bool lista_esta_vacia(const lista_t *lista);

// Agrega un nuevo elemento en la primera posicion de la lista.
// Pre: la lista fue creada.
// Post: se agregó un nuevo elemento al principio de la lista.
// Devuelve falso en caso de error y verdadero si todo sale correctamente.
bool lista_insertar_primero(lista_t *lista, void *dato);

// Agrega un nuevo elemento en la ultima posicion de la lista.
// Pre: la lista fue creada.
// Post: se agregó un nuevo elemento al final de la lista.
// Devuelve falso en caso de error y verdadero si todo sale correctamente.
bool lista_insertar_ultimo(lista_t *lista, void *dato);

// Obtiene el primer valor de la lista. Si la lista tiene elementos,
// se devuelve el primer valor. Si está vacía devuelve NULL.
// Pre: la lista fue creada.
// Post: se devolvió el valor del principio de la lista, cuando la lista no está
// vacía, NULL en caso contrario.
void *lista_ver_primero(const lista_t *lista);


// Obtiene el ultimo valor de la lista. Si la lista tiene elementos,
// se devuelve el ultimo valor. Si está vacía devuelve NULL.
// Pre: la lista fue creada.
// Post: se devolvió el valor del final de la lista, cuando la lista no está
// vacía, NULL en caso contrario.
void *lista_ver_ultimo(const lista_t* lista);

// Saca el elemento del princio de la lista. Si la lista tiene elementos, se quita el
// primero, y se devuelve ese valor. Si la lista está vacía, devuelve
// NULL.
// Pre: la lista fue creada.
// Post: si la lista no estaba vacía, se devuelve el valor del principio.
void *lista_borrar_primero(lista_t *lista);

// Devuelve el tamaño de la lista
// Pre: la lista fue creada
size_t lista_largo(const lista_t *lista);

// Itera la lista.
// Pre: la lista fue creada. Visitar es una función capaz de modificar
// los datos de la lista, o NULL en caso de que no se la utilice. Sera llamada por cada
// item de la lista. En caso de devolver "False" se irrumpira el iterador.
// Post: Si se recibe la función Visitar por parámetro,
// para cada uno de los elementos de la lista llama a Visitar hasta que la funcion devuelva "False"
// o se termine de iterar.
void lista_iterar(lista_t *lista, bool visitar(void *dato, void *extra), void *extra);

/* *****************************************************************
 *                    PRIMITIVAS DEL ITERADOR EXTERNO
 * *****************************************************************/

// Crea un iterador de la lista.
// Pre: la lista esta creada.
// Post: devuelve un nuevo iterador en la primera posición de la lista.
lista_iter_t* lista_iter_crear(lista_t *lista);

// Avanza a la siguiente posicion del iterador.
// Pre: el iterador esta creado.
// Post: mueve la referencia actual del iterador al siguiente elemento. Si
// el iterador se encuentra en el final entonces devuelve false, de lo contrario
// devuelve true.
bool lista_iter_avanzar(lista_iter_t *iter);

// Obtiene el valor en la posicion actual del iterador. Si el iterador no esta al final,
// se devuelve el valor. Si el iterador esta al final devuelve NULL.
// Pre: el iterador esta creado.
// Post: se devolvió el valor de la posicion actual del iterador.
void* lista_iter_ver_actual(const lista_iter_t *iter);

// Devuelve verdadero o falso, según si el iterador se encuentra al final.
// Pre: el iterador fue creado.
// Post: si el iterador esta al final devuelve verdadero, en el caso contrario 
// devuelve falso.
bool lista_iter_al_final(const lista_iter_t *iter);

// Destruye el iterador.
// Pre: el iterador fue creado.
// Post: el iterador fue destruido.
void lista_iter_destruir(lista_iter_t *iter);

// Inserta un elemento en la lista en la posicion del elemento actual del iterador.
// Pre: el iterador fue creado.
// Post: se inserta un elemento que va a tomar la posicion del elemento al que se apunta.
// Si la insercion es correcta el iterador apuntara al nuevo elemento y se devolvera true, en caso
// contrario se devolvera false y la referencia quedara intacta. Esta funcion modifica la lista.
bool lista_iter_insertar(lista_iter_t *iter, void *dato);

// Saca el elemento de la lista en la posicion actual del iterador y lo devuelve.
// Pre: el iterador fue creado.
// Post: si el iterador se encuentra al final devuelve NULL, en el caso contrario devuelve
// el elemento de la posicion actual y lo elimina de la lista.
void* lista_iter_borrar(lista_iter_t *iter);

/* *****************************************************************
 *                    PRUEBAS PARA LA lista
 * *****************************************************************/

// Realiza pruebas sobre la implementación de la lista del alumno.
//
// Las pruebas deben emplazarse en el archivo ‘pruebas_alumno.c’, y
// solamente pueden emplear la interfaz pública tal y como aparece en lista.h
// (esto es, las pruebas no pueden acceder a los miembros del struct lista).
//
// Para la implementación de las pruebas se debe emplear la función
// print_test(), como se ha visto en TPs anteriores.
void pruebas_lista_alumno(void);

#endif // LISTA_H
