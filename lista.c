#include "lista.h"
#include <stdlib.h>
#include <stdio.h>

struct nodo;
typedef struct nodo nodo_t;

struct nodo {
  void* dato;
  nodo_t* prox;
};

struct lista {
  nodo_t* prim; // Referencia al primer nodo de la lista
  nodo_t* ult; // Referencia al ultimo nodo de la lista
  size_t cantidad;
};

struct lista_iter {
  lista_t* lista;
  nodo_t* act;
  nodo_t* ant;
};

lista_t* lista_crear(void){
  lista_t* lista = malloc(sizeof(lista_t));

  if(!lista) {
    return NULL;
  }
  lista->prim = NULL;
  lista->ult = NULL;
  lista->cantidad = 0;
  return lista;
}

bool lista_esta_vacia(const lista_t *lista) {
  return lista->prim == NULL;
}

void* lista_ver_primero(const lista_t *lista) {
  return lista_esta_vacia(lista) ? NULL : lista->prim->dato;
}

void *lista_ver_ultimo(const lista_t* lista) {
  return lista_esta_vacia(lista) ? NULL : lista->ult->dato;
}

void* lista_borrar_primero(lista_t *lista){
  if(lista_esta_vacia(lista)) {
    return NULL;
  }

  nodo_t* nodo_actual = lista->prim;

  lista->prim = nodo_actual->prox;
  lista->ult = nodo_actual->prox ? lista->ult : NULL;

  void* dato = nodo_actual->dato;
  lista->cantidad --;
  free(nodo_actual);
  return dato;
}


nodo_t* lista_insertar_generico(lista_t *lista, void *dato, nodo_t* anterior, nodo_t* siguiente){
  nodo_t* nodo_nuevo = malloc(sizeof(nodo_t));

  if(nodo_nuevo == NULL) {
    return NULL;
  }

  nodo_nuevo->dato = dato;
  nodo_nuevo->prox = NULL;

  if(lista_esta_vacia(lista)) {
    lista->prim = nodo_nuevo;
    lista->ult = nodo_nuevo;
  } else {
    if(siguiente) {
      nodo_nuevo->prox = siguiente;
    }

    if(anterior) {
      anterior->prox = nodo_nuevo;
    }
  }

  lista->cantidad ++;
  return nodo_nuevo;
}

bool lista_insertar_primero(lista_t *lista, void *dato){
  nodo_t* nodo = lista_insertar_generico(lista, dato, NULL, lista->prim);
  if(!nodo) return false;
  lista->prim = nodo;
  return true;
}

bool lista_insertar_ultimo(lista_t *lista, void *dato){
  nodo_t* nodo = lista_insertar_generico(lista, dato, lista->ult, NULL);
  if(!nodo) return false;
  lista->ult = nodo;
  return true;
}

void lista_destruir(lista_t *lista, void destruir_dato(void*)) {
  while(!lista_esta_vacia(lista)) {
    void* dato = lista_borrar_primero(lista);
    if(destruir_dato != NULL){
      destruir_dato(dato);
    }
  }
  free(lista);
}

void lista_iterar(lista_t *lista, bool visitar(void *dato, void *extra), void *extra) {
  nodo_t* actual = lista->prim;
  if(!visitar) return;
  while(actual) {
    if(!visitar(actual->dato, extra)) return;
    actual = actual->prox;
  }
}

size_t lista_largo(const lista_t *lista) {
  return lista->cantidad;
}

lista_iter_t* lista_iter_crear(lista_t *lista) {
  lista_iter_t* iter = malloc(sizeof(lista_iter_t));

  if(!iter) {
    return NULL;
  }
  iter->lista = lista;
  iter->act = lista->prim;
  iter->ant = NULL;
  return iter;
}

bool lista_iter_al_final(const lista_iter_t *iter) {
  return iter->act == NULL;
}

bool lista_iter_avanzar(lista_iter_t *iter) {
  if(lista_iter_al_final(iter)) return false;
  iter->ant = iter->act;
  iter->act = iter->act->prox;
  return true;
}

void* lista_iter_ver_actual(const lista_iter_t *iter) {
  return iter->act ? iter->act->dato : NULL;
}

void lista_iter_destruir(lista_iter_t *iter) {
  free(iter);
}

bool lista_iter_insertar(lista_iter_t* iter, void *dato) {
  // Se encuentra al final
  if(lista_iter_al_final(iter)) {
    if (!lista_insertar_ultimo(iter->lista, dato)) return false;
    iter->act = iter->lista->ult;
  } else if(iter->act && !iter->ant) {
    if (!lista_insertar_primero(iter->lista, dato)) return false;
    iter->act = iter->lista->prim;
  } else {
    nodo_t* nodo = lista_insertar_generico(iter->lista, dato, iter->ant, iter->act);
    if(!nodo) return false;
    iter->act = nodo;
  }

  return true;
}

void* lista_iter_borrar(lista_iter_t* iter) {
  // En caso de que la lista este vacia (no hay iter->act ni iter->ant) o
  // la lista este al final (no hay iter->act)
  if(lista_esta_vacia(iter->lista) || lista_iter_al_final(iter)) {
    return NULL;
  } else if(!iter->ant) {
    // En caso de que no hay iter->ant significa que esta al principio
    iter->act = iter->act->prox;
    return lista_borrar_primero(iter->lista);
  } else if(!iter->act->prox){
    // En caso de que no hay iter->act->prox significa que esta al final
    nodo_t* nodo = iter->act;
    void* dato = nodo->dato;
    iter->ant->prox = NULL;
    iter->lista->ult = iter->ant;
    iter->lista->cantidad --;
    free(nodo);
    iter->act = NULL;
    return dato;
  } else {
    // Significa que no estamos en un edge case.
    nodo_t* nodo = iter->act;
    void* dato = nodo->dato;
    iter->act = iter->act->prox;
    iter->ant->prox = iter->act;
    iter->lista->cantidad --;
    free(nodo);
    return dato;
  }
}
